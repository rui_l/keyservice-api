package com.routard.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.routard.utils.Validator;
import lombok.*;

@Getter
@Setter
@Builder
public class MailToSend {
    @JsonProperty(index = 1)
    private String recipient;
    @JsonProperty(index = 2)
    private String subject;
    @JsonProperty(index = 3)
    private String content;

    public boolean isValid(){
        return Validator.isValidEmailAddress(recipient);
    }
}
