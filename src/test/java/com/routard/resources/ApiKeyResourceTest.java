package com.routard.resources;

import com.routard.dto.ClientToCreateDTO;
import com.routard.dto.MailHistoryDTO;
import com.routard.dto.MailServiceClientDTO;
import com.routard.dto.MailToSend;
import com.routard.entities.ClientEntity;
import com.routard.entities.MailHistoryEntity;
import com.routard.repositories.ClientRepository;
import com.routard.repositories.MailHistoryRepository;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.MediaType;
import org.graalvm.nativeimage.Platform;
import org.jboss.resteasy.spi.HttpResponseCodes;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ApiKeyResourceTest {
    @Inject
    ClientRepository clientRepository;

    @Inject
    MailHistoryRepository mailHistoryRepository;

    static ClientEntity clientEntity;

    private static final String APIKEY_OF_KEYSERVICE = "Q3pzZDlNQVk1MHFD";
    private static final String CLIENT_NAME = "KeyService";

    private static final String MAILTO = "test@ex.com";
    @Test
    @Order(1)
    void getClientByEmptyKey() {
        String apiKey=" ";
        given()
                .pathParam("apiKey", apiKey)
                .when().get("/apikey/{apiKey}")
                .then().statusCode(HttpResponseCodes.SC_BAD_REQUEST);
    }

    @Test
    @Order(2)
    void getClientByApiKeyOfFakeClient() {
        String apiKey="FakeApiKey";
        given()
                .pathParam("apiKey", apiKey)
                .when().get("/apikey/{apiKey}")
                .then().statusCode(HttpResponseCodes.SC_NOT_FOUND);
    }
    @Test
    @Order(3)
    void getClientByApiKey() {
        MailServiceClientDTO client= given()
                .pathParam("apiKey", APIKEY_OF_KEYSERVICE)
                .when().get("/apikey/{apiKey}")
                .then().statusCode(HttpResponseCodes.SC_OK)
                .extract().as(MailServiceClientDTO.class);
        assert client.getNomClient().equalsIgnoreCase(CLIENT_NAME);
    }

    @Test
    @Order(4)
    void getMailCountByEmptyKey() {
        String apiKey=" ";
        given()
                .pathParam("apiKey", apiKey)
                .when().get("/apikey/{apiKey}/mailCount")
                .then().statusCode(HttpResponseCodes.SC_BAD_REQUEST);
    }

    @Test
    @Order(5)
    void getMailCountByApiKeyOfFakeClient() {
        String apiKey="FakeApiKey";
        given()
                .pathParam("apiKey", apiKey)
                .when().get("/apikey/{apiKey}/mailCount")
                .then().statusCode(HttpResponseCodes.SC_NOT_FOUND);
    }


    @Test
    @Order(6)
    void saveMailWithEmptyKey(){
        String apiKey=" ";
        MailToSend mailToSend = MailToSend.builder().build();
        given()
                .pathParam("apiKey", apiKey)
                .when().contentType(MediaType.APPLICATION_JSON)
                .post("/apikey/{apiKey}/mails")
                .then().contentType(MediaType.TEXT_PLAIN)
                .statusCode(HttpResponseCodes.SC_BAD_REQUEST)
                .body(containsStringIgnoringCase("apikey"));
    }

    @Test
    @Order(7)
    void saveMailWithoutMailToSend() {
        given()
                .pathParam("apiKey", APIKEY_OF_KEYSERVICE)
                .when().contentType(MediaType.APPLICATION_JSON)
                .post("/apikey/{apiKey}/mails")
                .then().contentType(MediaType.TEXT_PLAIN)
                .statusCode(HttpResponseCodes.SC_BAD_REQUEST)
                .body(containsStringIgnoringCase("email"));
    }

    @Test
    @Order(8)
    void saveMailWithInvalidEmail() {
        MailToSend mailToSend = MailToSend.builder().build();
        mailToSend.setRecipient("badFormatEmailAddress");
        mailToSend.setSubject("Test of invalid email address");
        mailToSend.setContent("Content");

        given().pathParam("apiKey", APIKEY_OF_KEYSERVICE)
                .when().contentType(MediaType.APPLICATION_JSON).body(mailToSend)
                .post("/apikey/{apiKey}/mails")
                .then().contentType(MediaType.TEXT_PLAIN)
                .statusCode(HttpResponseCodes.SC_BAD_REQUEST)
                .body(containsStringIgnoringCase("email"));
    }

    @Test
    @Order(9)
    void saveMailWithApiKeyOfFakeClient() {
        MailToSend mailToSend = MailToSend.builder()
                .recipient(MAILTO)
                .subject("Test with Fake Client")
                .content("Content")
                .build();

        String apiKey="FakeApiKey";

        given().pathParam("apiKey", apiKey)
                .when().contentType(MediaType.APPLICATION_JSON)
                .body(mailToSend)
                .post("/apikey/{apiKey}/mails")
                .then().contentType(MediaType.TEXT_PLAIN)
                .statusCode(HttpResponseCodes.SC_NOT_FOUND)
                .body(containsStringIgnoringCase("client"));
    }
    @Test
    @Order(10)
    void saveMail() {
        MailToSend mailToSend = MailToSend.builder()
                .recipient(MAILTO)
                .subject("Test of saveMail in ApiKeyService")
                .content("Content")
                .build();

        MailHistoryDTO mailHistoryDTO = given()
                .pathParam("apiKey", APIKEY_OF_KEYSERVICE)
                .when().contentType(MediaType.APPLICATION_JSON)
                .body(mailToSend)
                .post("/apikey/{apiKey}/mails")
                .then().contentType(MediaType.APPLICATION_JSON)
                .statusCode(HttpResponseCodes.SC_CREATED)
                .extract().as(MailHistoryDTO.class);

        LocalDate localDate = LocalDate.now();

        assert mailHistoryDTO.getId() != 0;
        assert mailHistoryDTO.getDestinataire().equals(mailToSend.getRecipient());
        assert mailHistoryDTO.getObjetMail().equals(mailToSend.getSubject());
        assert mailHistoryDTO.getClient().getNom().equalsIgnoreCase(CLIENT_NAME);
        assert mailHistoryDTO.getDateEnvoi().toLocalDate().isEqual(localDate);
    }


    private static Integer idForMailcountTest = -1;
    private static String keyForMailcountTest = "";
    private static Integer countForMailcountTest = 1;

    @Test
    @Order(11)
    @Transactional
    void createTestAccount() {
        String nomClient = "TestAccount";
        Integer quota = 1;
        ClientEntity client = new ClientEntity(nomClient, MAILTO, quota);
        client.setCle("azerty");
        clientRepository.persistAndFlush(client);

        idForMailcountTest=client.getId();
        keyForMailcountTest = client.getCle();

        assert client.getId() !=0;
        System.out.println("idForMailcountTest : " + idForMailcountTest);
        System.out.println("keyForMailcountTest : " + keyForMailcountTest);

    }

    @Test
    @Order(12)
    @Transactional
    void saveTestAccountMail() {
        MailHistoryEntity mail = new MailHistoryEntity();
        mail.setObjetMail("an email from TestAccount");
        mail.setDestinataire(MAILTO);
        mail.setDateEnvoi(LocalDateTime.now());
        clientEntity = clientRepository.findById(idForMailcountTest);
        mail.setClientEntity(clientEntity);

        mailHistoryRepository.persistAndFlush(mail);
        assert mail.getId() != 0;

        /*
        MailToSend mailToSend = MailToSend.builder()
                .recipient(MAILTO)
                .subject("Sending an email to test mailCount")
                .content("Content")
                .build();

        MailHistoryDTO mailHistoryDTO = given()
                .pathParam("apiKey", keyForMailcountTest)
                .when().contentType(MediaType.APPLICATION_JSON)
                .body(mailToSend)
                .post("/apikey/{apiKey}/mails")
                .then().contentType(MediaType.APPLICATION_JSON)
                .statusCode(HttpResponseCodes.SC_CREATED)
                .extract().as(MailHistoryDTO.class);

        System.out.println("Client : "+mailHistoryDTO.getClient().getNom()+" with ID_CLIENT : "+mailHistoryDTO.getClient().getId());
        System.out.println("Objet : "+mailHistoryDTO.getObjetMail());
        System.out.println("From : "+mailHistoryDTO.getDestinataire());


         */
    }

    @Test
    @Order(13)
    void getMailCount() {
        given()
                .pathParam("apiKey", keyForMailcountTest)
                .when().get("/apikey/{apiKey}/mailCount")
                .then().contentType(MediaType.TEXT_PLAIN).statusCode(HttpResponseCodes.SC_OK)
                .body(equalTo(String.valueOf(countForMailcountTest)));
    }

    @Test
    @Order(14)
    @Transactional
    void deleteTestAccount() {
        if (idForMailcountTest !=-1) {
            clientRepository.deleteById(idForMailcountTest);
            System.out.println("ID_CLIENT : "+idForMailcountTest+" deleted !");
        }
    }

    @Test
    @Order(15)
    @Transactional
    void removeTestMailto() {
        List<MailHistoryEntity> mailHistoryEntities = mailHistoryRepository.find("destinataire=?1", MAILTO).list();
        for (MailHistoryEntity mail:mailHistoryEntities) {
            mailHistoryRepository.deleteById(mail.getId());
        }
    }


}