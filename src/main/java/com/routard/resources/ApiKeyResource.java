package com.routard.resources;

import com.routard.dto.ClientDTO;
import com.routard.dto.MailServiceClientDTO;
import com.routard.dto.MailToSend;
import com.routard.dto.MailHistoryDTO;
import com.routard.entities.ClientEntity;
import com.routard.entities.MailHistoryEntity;
import com.routard.repositories.ClientRepository;
import com.routard.repositories.MailHistoryRepository;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.*;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import java.time.LocalDateTime;

@Path("/apikey")
@Tag(name = "ApiKey")
@Produces(MediaType.APPLICATION_JSON)
public class ApiKeyResource {

    /*@Context
    UriInfo uriInfo;*/
    @Inject
    ClientRepository clientRepository;
    @Inject
    MailHistoryRepository mailHistoryRepository;
    @GET
    @Operation(summary = "Find client by ApiKey", description = "Find the client with this ApiKey.")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{apiKey}")
    public Response getClientByApiKey(@PathParam(value = "apiKey") String apiKey) {
//        System.out.println("dans getClientByApiKey");
        if (apiKey == null || apiKey.trim().isEmpty())
            return Response.ok("ApiKey required!", MediaType.TEXT_PLAIN).status(Response.Status.BAD_REQUEST).build();
        ClientEntity clientEntity = clientRepository.findByCle(apiKey);
        if (clientEntity == null)
            return Response.ok("Client account not found.", MediaType.TEXT_PLAIN).status(Response.Status.NOT_FOUND).build();
        MailServiceClientDTO mailServiceClientDTO = new MailServiceClientDTO();
        mailServiceClientDTO.setId(clientEntity.getId());
        mailServiceClientDTO.setNomClient(clientEntity.getNomClient());
        mailServiceClientDTO.setQuotaMensuel(clientEntity.getQuotaMensuel());
        mailServiceClientDTO.setStatut(clientEntity.getStatut());

        return Response.ok(mailServiceClientDTO).build();
    }

    @GET
    @Operation(summary = "Count mails", description = "Return the quantity of sent mails with this ApiKey.")
    @Produces(MediaType.TEXT_PLAIN)
    @Path("{apiKey}/mailCount")
    public Response getMailCount(@PathParam(value = "apiKey") String apiKey) {
//        System.out.println("apikey=" + apiKey);
        if (apiKey == null || apiKey.trim().isEmpty())
            return Response.ok("ApiKey required!", MediaType.TEXT_PLAIN).status(Response.Status.BAD_REQUEST).build();
        ClientEntity clientEntity = clientRepository.findByCle(apiKey);
        if (clientEntity == null)
            return Response.ok("Client account not found.", MediaType.TEXT_PLAIN).status(Response.Status.NOT_FOUND).build();
        Integer mailCount = mailHistoryRepository.getMailCount(clientEntity);
//        return Response.ok(String.format("You have already sent %d mails", mailCount)).build();
        return Response.ok(mailCount.toString()).build();
    }

    @Transactional
    @POST
    @Operation(summary = "Record mails", description = "Record an email in database.")
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{apiKey}/mails")
    public Response saveMail(@PathParam(value = "apiKey") String apiKey, MailToSend mailToSend) {
        if (apiKey == null || apiKey.trim().isEmpty())
            return Response.ok("ApiKey required!", MediaType.TEXT_PLAIN).status(Response.Status.BAD_REQUEST).build();
        if ( mailToSend == null || !mailToSend.isValid())
            return Response.ok("Email address should be valid!", MediaType.TEXT_PLAIN).status(Response.Status.BAD_REQUEST).build();
        ClientEntity clientEntity = clientRepository.findByCle(apiKey);
        if (clientEntity == null)
            return Response.ok("Client account not found.", MediaType.TEXT_PLAIN).status(Response.Status.NOT_FOUND).build();
        MailHistoryEntity mailHistoryEntity = new MailHistoryEntity();
        mailHistoryEntity.setObjetMail(mailToSend.getSubject());
        mailHistoryEntity.setDestinataire(mailToSend.getRecipient());
        mailHistoryEntity.setClientEntity(clientEntity);

        LocalDateTime date = LocalDateTime.now();
        mailHistoryEntity.setDateEnvoi(date);

        try{
        mailHistoryRepository.persist(mailHistoryEntity);
        return Response.ok(new MailHistoryDTO(mailHistoryEntity)).status(Response.Status.CREATED).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.ok("Recording failed.", MediaType.TEXT_PLAIN).status(Response.Status.INTERNAL_SERVER_ERROR).build();
//            throw new RuntimeException(e);
        }

        /*UriBuilder uriBuilder = uriInfo.getRequestUriBuilder();
        uriBuilder.path(apiKey);
        return Response.created(uriBuilder.build()).entity(new MailHistoryDTO(mailHistoryEntity)).build();*/
//        return (new MailHistoryDTO(mailHistoryEntity));
    }
}
