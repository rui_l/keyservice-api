package com.routard.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorTest {

    @Test
    void TestValidEmailAddress() {
        assertTrue(Validator.isValidEmailAddress("test@monsite.com"));
        assertTrue(Validator.isValidEmailAddress("test.user@for_example.fr"));
        assertTrue(Validator.isValidEmailAddress("user-name_login@site.domaine.org"));
        assertTrue(Validator.isValidEmailAddress("1+m%F-_@fortyeight.co"));

        assertFalse(Validator.isValidEmailAddress("test@com"));
        assertFalse(Validator.isValidEmailAddress("user@.c."));
        assertFalse(Validator.isValidEmailAddress("test.example.net"));
        assertFalse(Validator.isValidEmailAddress("@example.fr"));
        assertFalse(Validator.isValidEmailAddress("user@test@1.co"));
        assertFalse(Validator.isValidEmailAddress("ùltI?_^M@fake.p"));
    }
}