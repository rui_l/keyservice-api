package com.routard.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.routard.utils.Validator;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ClientToCreateDTO {
    @JsonProperty(index = 1)
    @Setter(AccessLevel.NONE)
    private Integer id;
    @JsonProperty(index = 2)
    private String nomClient;
    @JsonProperty(index = 3)
    private String adresseMail;

//    @JsonIgnore
    public boolean isValidClientToCreate() {
        if (Validator.isValidEmailAddress(adresseMail)) {
            return !nomClient.equalsIgnoreCase(adresseMail);
        }
        return false;
    }

}
