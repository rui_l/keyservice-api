package com.routard.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class MailServiceClientDTO {
    private Integer id;
    private String nomClient;
    private int quotaMensuel;
    private String statut;
}
